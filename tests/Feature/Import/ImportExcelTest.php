<?php

namespace Tests\Feature\Import;

use App\User\Models\User;
use Database\Factories\UserFactory;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Queue;
use Symfony\Component\HttpFoundation\Response as ResponseAlias;
use Tests\TestCase;

class ImportExcelTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();

        Queue::setDefaultDriver('sync');
    }

    public function test_if_excel_file_is_imported(): void
    {
        $pathToFile = base_path('tests/Feature/Import/test-feature.xlsx');

        $this->assertFileExists($pathToFile);

        $testFile = new UploadedFile(
            $pathToFile,
            'test.xlsx',
            'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            null,
            true,
        );

        UserFactory::new()->create();

        $this->actingAs(User::first())
            ->post(route('api.imports.upload'), [
                'file' => $testFile,
                'type' => 'rows',
            ])
            ->assertStatus(ResponseAlias::HTTP_ACCEPTED);

        $this->assertDatabaseCount('rows', 2);
    }
}
