<?php

namespace App\Row\Http\Controllers;


use App\Row\Repositories\RowRepositoryInterface;
use Illuminate\Contracts\View\View as ViewContract;
use Illuminate\Support\Facades\View;

class RowController
{
    public function __construct(
        private readonly RowRepositoryInterface $rowRepository,
    ) {
    }

    public function index(): ViewContract
    {
        $rows = $this->rowRepository->allGroupedByDate();

        return View::make('rows', [
            'items' => $rows->toArray(),
        ]);
    }
}