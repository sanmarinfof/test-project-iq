<?php

namespace App\Import\Jobs;

use App\Import\Enums\ImportableType;
use App\Import\Services\ImportService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\App;

class ImportJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public function __construct(
        private readonly string $disk,
        private readonly string $filePath,
        private readonly ImportableType $importableType,
    )
    {
    }

    public function handle(): void
    {
        App::make(ImportService::class)->import($this->importableType, $this->disk, $this->filePath);
    }
}
