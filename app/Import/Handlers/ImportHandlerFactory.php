<?php

namespace App\Import\Handlers;

use App\Import\Enums\ImportableType;

class ImportHandlerFactory
{
    public function make(ImportableType $type): AbstractImportHandler
    {
        return match ($type) {
            ImportableType::Rows => new RowsImportHandler(),
        };
    }
}