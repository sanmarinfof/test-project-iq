<?php

namespace App\Import\Handlers;

use App\Row\Models\Row;
use DateInterval;
use DateTime;

class RowsImportHandler extends AbstractImportHandler
{
    /**
     * @throws \Exception
     */
    public function model(array $row): Row
    {
        $date = $row['date'];
        if (is_float($date) || is_numeric($date)) {
            if ($date > 60) {
                $date--;
            }
            // The base date for Excel is 1900-01-01
            $baseDate = DateTime::createFromFormat('Y-m-d', '1899-12-31');
            // Add the Excel date (number of days since base date)
            $dateInterval = new DateInterval(sprintf('P%dD', $date));
            $baseDate->add($dateInterval);
            $date = $baseDate->format('Y-m-d');
        }

        return new Row([
            'id' => intval($row['id']) + $this->getRowNumber() - 1,
            'name' => $row['name'],
            'date' => $date,
        ]);
    }
}