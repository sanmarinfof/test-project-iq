<?php

use App\Import\Http\Controllers\ImportController;
use Illuminate\Support\Facades\Route;

Route::post('imports/upload', [ImportController::class, 'upload'])->name('api.imports.upload');
