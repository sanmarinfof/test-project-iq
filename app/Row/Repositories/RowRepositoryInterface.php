<?php

namespace App\Row\Repositories;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

interface RowRepositoryInterface
{
    public function all(array $columns = ['*'], array $relations = []): Collection;

    public function find(int $id, array $columns = ['*'], array $relations = []): Collection;

    public function allGroupedByDate(array $columns = ['*'], array $relations = [],): Collection;

    public function paginate(array $columns = ['*'], array $relations = [], int $perPage = 15): LengthAwarePaginator;
}