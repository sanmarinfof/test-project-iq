<?php

namespace App\Infrastructure\Providers;

use App\Infrastructure\Repositories\RowRepository;
use App\Row\Repositories\RowRepositoryInterface;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        $this->app->bind(
            RowRepositoryInterface::class,
            RowRepository::class,
        );
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        //
    }
}
