<?php

namespace App\Row\Models;

use App\Events\RowCreated;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Event;

class Row extends Model
{
    protected $fillable = [
        'id',
        'name',
        'date',
    ];

    protected $casts = [
        'date' => 'date:Y-m-d',
    ];

    protected static function boot(): void
    {
        parent::boot();

        static::created(function (Row $row) {
            Event::dispatch(new RowCreated($row));
        });
    }
}
