<?php

namespace App\Import\Enums;

enum ImportableType: string
{
    case Rows = 'rows';
}