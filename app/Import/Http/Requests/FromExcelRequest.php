<?php

namespace App\Import\Http\Requests;

use App\Import\Enums\ImportableType;
use App\Infrastructure\Http\Requests\BaseRequest;
use Illuminate\Validation\Rule;

class FromExcelRequest extends BaseRequest
{
    public function rules(): array
    {
        return [
            'file' => [
                'required',
                Rule::file()->max(2048),
                'mimes:xlsx,xls',
            ],
            'type' => [
                'required',
                Rule::enum(ImportableType::class),
            ]
        ];
    }
}
