<?php

namespace App\Import\Services;

use App\Import\Enums\ImportableType;
use App\Import\Handlers\ImportHandlerFactory;

class ImportService
{
    public function import(ImportableType $importableType, string $disk, string $filePath): void
    {
        $handlerFactory = new ImportHandlerFactory();
        $handlerFactory->make($importableType)->import($filePath, $disk);
    }
}