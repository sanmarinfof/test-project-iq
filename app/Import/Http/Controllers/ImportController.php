<?php

namespace App\Import\Http\Controllers;

use App\Import\Enums\ImportableType;
use App\Import\Http\Requests\FromExcelRequest;
use App\Import\Jobs\ImportJob;
use App\Infrastructure\Http\Controllers\Controller;
use App\Infrastructure\Services\FileService;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpFoundation\Response as ResponseAlias;

class ImportController extends Controller
{
    public function __construct(
        private readonly FileService $fileService,
    ) {
    }

    public function upload(FromExcelRequest $request)
    {
        $filePath = $this->fileService->tempSaveUploadedFile($request->file('file'));

        ImportJob::dispatch('temp', $filePath, $request->enum('type', ImportableType::class));

        return new JsonResponse([], ResponseAlias::HTTP_ACCEPTED);
    }
}
