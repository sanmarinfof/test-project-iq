<?php

use App\Row\Http\Controllers\RowController;

\Illuminate\Support\Facades\Route::get('rows', [RowController::class, 'index'])->name('web.rows.index');