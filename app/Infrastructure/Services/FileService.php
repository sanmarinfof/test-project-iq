<?php

namespace App\Infrastructure\Services;

use Illuminate\Http\UploadedFile;

class FileService
{
    public function tempSaveUploadedFile(UploadedFile $file): string
    {
        $randomName = uniqid().'.'.$file->getClientOriginalExtension();

        return $file->storeAs('/', $randomName, ['disk' => 'temp']);
    }
}